package health

import (
	"crypto/tls"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"time"
)

var checkFuncs map[string]func() (int, error)

func RegisterHealthCheck(name string, check func() (int, error)) error {
	if checkFuncs == nil {
		checkFuncs = map[string]func() (int, error){}
	}
	if checkFuncs[name] != nil {
		return fmt.Errorf("HealthCheck `%s` Registered More Than Once!", name)
	}
	log.Printf("added %s", name)
	checkFuncs[name] = check
	return nil
}

func Check(w http.ResponseWriter, r *http.Request) {
	for name, function := range checkFuncs {
		i, err := function()
		if err != nil {
			w.WriteHeader(i)
			if r.Method == "GET" {
				w.Write([]byte(fmt.Sprintf("HealthCheck Error (%s): %s\n", name, err)))
			}
			return
		}
	}
	w.WriteHeader(200)
}
func Healthcheck(args []string, port string, https bool) (int, error) {
	fs := flag.NewFlagSet("healthcheck", flag.ContinueOnError)
	skipweb := fs.Bool("w", false, "skip webcheck")
	fs.Parse(args)
	client := &http.Client{
		Timeout: time.Second * 15,
		Transport: &http.Transport{
			DisableCompression: true,
			MaxIdleConns:       5,
			IdleConnTimeout:    15 * time.Second,
			DisableKeepAlives:  true,
			DialContext: (&net.Dialer{
				Timeout:   15 * time.Second,
				KeepAlive: 15 * time.Second,
				DualStack: false,
			}).DialContext,
		},
	}
	if !*skipweb {
		scheme := "http"
		if https {
			scheme = "https"
			client.Transport.(*http.Transport).TLSClientConfig = &tls.Config{
				InsecureSkipVerify: true,
			}
		}
		res, err := client.Get(scheme + "://127.0.0.1:" + port + "/health")
		if err != nil {
			return 1, err
		}
		defer res.Body.Close()
		b, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return 1, err
		}

		if res.StatusCode != 200 {
			return 1, fmt.Errorf("Http Check Returned: %d - %s", res.StatusCode, string(b))
		}
	}
	client.Transport.(*http.Transport).CloseIdleConnections()
	return 0, nil
}
