# go-health
[![GoDoc](https://godoc.org/gitlab.com/jckimble/go-health?status.svg)](https://godoc.org/gitlab.com/jckimble/go-health)
[![pipeline status](https://gitlab.com/jckimble/go-health/badges/master/pipeline.svg)](https://gitlab.com/jckimble/go-health/commits/master)
[![coverage report](https://gitlab.com/jckimble/go-health/badges/master/coverage.svg)](https://gitlab.com/jckimble/go-health/commits/master)

Package `jckimble/go-health` implements a healthcheck for using golang and docker healthchecks easily.

---
* [Install](#install)
* [Examples](#examples)

---

## Install
```sh
go get -u gitlab.com/jckimble/go-health
```

## Examples
Basic http check
```go

func init(){
	if len(os.Args[1:])> 0{
		code,err:=health.Healthcheck(os.Args[2:],"8080",false)
		if err!=nil{
			log.Printf("%s",err)
		}
		os.Exit(code)
	}
}

func main(){
	r := mux.NewRouter()
	r.HandleFunc("/health", health.Check).Host("127.0.0.1").Methods("GET")
	log.Fatal(http.ListenAndServe(":8080",r))
}

```
Basic https check
```go

func init(){
	if len(os.Args[1:])> 0{
		code,err:=health.Healthcheck(os.Args[2:],"8443",true)
		if err!=nil{
			log.Printf("%s",err)
		}
		os.Exit(code)
	}
}

func main(){
	r := mux.NewRouter()
	r.HandleFunc("/health", health.Check).Host("127.0.0.1").Methods("GET")
	log.Fatal(http.ListenAndServe(":8443","cert.pem","key.pem",r))
}

```
Custom Healthcheck
```go

func init(){
	if len(os.Args[1:])> 0{
		code,err:=health.Healthcheck(os.Args[2:],"8080",false)
		if err!=nil{
			log.Printf("%s",err)
		}
		os.Exit(code)
	}
}

func stubHealthCheck() (int,error){
	return 500,fmt.Errorf("This is a stub")
}

func main(){

	err:=health.RegisterHealthCheck("Stub",stubHealthCheck)
	if err!= nil{
		log.Fatal(err)
	}

	r := mux.NewRouter()
	r.HandleFunc("/health", health.Check).Host("127.0.0.1").Methods("GET")
	log.Fatal(http.ListenAndServe(":8080",r))
}

```

## License

Copyright 2018 James Kimble

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
