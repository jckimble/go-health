package health_test

import "testing"
import "net/http/httptest"
import "fmt"
import "gitlab.com/jckimble/go-health"

func TestRegister(t *testing.T) {
	err := health.RegisterHealthCheck("test", func() (int, error) {
		return 0, nil
	})
	if err != nil {
		t.Fatalf("Expected nil got %s", err)
	}
	err = health.RegisterHealthCheck("test", func() (int, error) {
		return 0, nil
	})
	if err == nil {
		t.Fatalf("Expected error got nil")
	}
	err = health.RegisterHealthCheck("test2", func() (int, error) {
		return 0, nil
	})
	if err != nil {
		t.Fatalf("Expected nil got %s", err)
	}
}

func TestCheck(t *testing.T) {
	r := httptest.NewRequest("GET", "http://127.0.0.1/health", nil)
	w := httptest.NewRecorder()
	health.Check(w, r)
	resp := w.Result()
	if resp.StatusCode != 200 {
		t.Fatalf("Expected 200 got %d", resp.StatusCode)
	}
	w = httptest.NewRecorder()
	health.RegisterHealthCheck("fail", func() (int, error) {
		return 500, fmt.Errorf("error")
	})
	health.Check(w, r)
	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Fatalf("Expected 500 got %d", resp.StatusCode)
	}
}
func TestHealthcheck(t *testing.T) {
	i, err := health.Healthcheck([]string{"-w"}, "8080", false)
	if err != nil {
		t.Fatalf("Expected nil got %s", err)
	}
	if i != 0 {
		t.Fatalf("Expected 0 got %d", i)
	}
	i, err = health.Healthcheck([]string{}, "8080", false)
	if err == nil {
		t.Fatalf("Expected error got nil")
	}
	if i != 1 {
		t.Fatalf("Expected 1 got %d", i)
	}
	i, err = health.Healthcheck([]string{}, "8443", true)
	if err == nil {
		t.Fatalf("Expected error got nil")
	}
	if i != 1 {
		t.Fatalf("Expected 1 got %d", i)
	}
}
